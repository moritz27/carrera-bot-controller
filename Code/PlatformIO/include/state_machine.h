#include <Arduino.h>
#include "config.h"
#include "display_control.h"
#include "server_control.h"

extern uint8_t sensor_counter;
extern int8_t max_sensors;

extern uint8_t lap_counter;
extern uint32_t time_lap;
extern uint32_t time_lap_longest;
extern uint32_t time_lap_shortest;

extern uint8_t lap_counter_human;
extern uint32_t time_lap_human;
extern uint32_t time_lap_longest_human;
extern uint32_t time_lap_shortest_human;


void state_machine_setup();
void state_machine_reset();
void state_machine_handler();