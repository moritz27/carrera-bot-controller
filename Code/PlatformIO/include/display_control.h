#include <Arduino.h>
#include "LiquidCrystal_I2C_ESP.h"

void display_setup();
void print_display_case_wait_for_init(int8_t max_sensors, int8_t sensor_counter, int16_t analog_value);
void print_display_sensor_init(int8_t sensor_counter, int16_t analog_value);
void print_display_init_rdy(int8_t sensor_counter/*, int8_t temp_counter*/);
void print_display_increase_to_max_speed(int8_t sensor_counter, int8_t temp_counter);
void print_display_wait_for_start();
void print_display_wait_for_race();
int8_t print_display_start_the_race();
void print_display_race(uint8_t lap_counter, uint32_t time_lap, uint8_t lap_counter_human, uint32_t time_lap_human /*, int16_t section*/);
void print_display_update_race(uint8_t lap_counter, uint32_t time_lap, uint8_t lap_counter_human, uint32_t time_lap_human /*, int16_t section*/);
void print_display_finish_race(uint8_t lap_counter, uint8_t lap_counter_human);
void print_display_wait_after_reset();