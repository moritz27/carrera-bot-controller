//#include <ESP8266WebServer.h>
#include "html_file.h" 

extern bool webserver_button_start;
extern bool webserver_button_restart;

void listVariables();
void defaultPage();
void toggleStart();
void toggleRestart();
void server_setup();
void server_handler();