#include "server_control.h"
#include"state_machine.h"

ESP8266WebServer server;

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Defined Variables #################################################
//----------------------------------------------------------------------------------------------------------------------------------
 
bool webserver_button_start = LOW;      // flag to start race via web server
bool webserver_button_restart = LOW;    // flag to restart race via web server

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Functions #################################################
//----------------------------------------------------------------------------------------------------------------------------------

// uncomment this section to send data to the HTML Page. But be aware of some time problems with the rest of the code can occure
void listVariables()
{
	// the order of the variables loaded to the html-site is important to get them at the right position
	// the function string temp contain all concatinated values and sends them as an string to the html-Site
	/*String temp = "[";
	temp += (String)"{\"name\":\"" + " sensors " + "\",\"wert\":\"" + (sensor_counter-1) + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " max sensors " + "\",\"wert\":\"" + (max_sensors) + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " laps " + "\",\"wert\":\"" + (lap_counter) + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " lap time " + "\",\"wert\":\"" + (time_lap/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " longest lap " + "\",\"wert\":\"" + (time_lap_longest/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " shortest lap " + "\",\"wert\":\"" + (time_lap_shortest/1000.00) + "s" + "\"}";
	temp += ',';*/
	/*temp += (String)"{\"name\":\"" + " 1st section " + "\",\"wert\":\"" + (time_sections[1]/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 2nd section " + "\",\"wert\":\"" + (time_sections[2]/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 3rd section " + "\",\"wert\":\"" + (time_sections[3]/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 4th section " + "\",\"wert\":\"" + (time_sections[4]/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 1st difference " + "\",\"wert\":\"" + (time_sections_difference[1]) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 2nd difference " + "\",\"wert\":\"" + (time_sections_difference[2]) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 3rd difference " + "\",\"wert\":\"" + (time_sections_difference[3]) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " 4th difference " + "\",\"wert\":\"" + (time_sections_difference[4]) + "s" + "\"}";
	temp += ',';*/
	/*temp += (String)"{\"name\":\"" + " laps human " + "\",\"wert\":\"" + (lap_counter_human) + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " lap time human " + "\",\"wert\":\"" + (time_lap_human/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " longest lap human" + "\",\"wert\":\"" + (time_lap_longest_human/1000.00) + "s" + "\"}";
	temp += ',';
	temp += (String)"{\"name\":\"" + " shortest lap human" + "\",\"wert\":\"" + (time_lap_shortest_human/1000.00) + "s" + "\"}";
	*/
	/*
	server.send(200, "application/json", temp += "]");
	Serial.println("values for html");Serial.println(temp);*/
}

// Starts web page on 192.168.4.1/
void defaultPage()
{
  server.send_P(200,"text/html", webpage);
}

// Read start Button on web server to set start value
void toggleStart()
{
  webserver_button_start = HIGH;
  server.send_P(200,"text/html", webpage);
  Serial.println("web server start button pushed");Serial.println(webserver_button_start);
}

// Read restart Button on web server to set restart value
void toggleRestart()
{
  webserver_button_restart = HIGH;
  server.send_P(200,"text/html", webpage);
  Serial.println("webserver restart button pushed");Serial.println(webserver_button_restart);
}

// setup the web page
void server_setup()
{
    // Setup Webserver
    server.on("/", defaultPage);                 
    server.on("/start",toggleStart);
    server.on("/restart",toggleRestart);
    //server.on("/lots",listVariables);
    server.begin();
}

// server handler
void server_handler()
{
    server.handleClient();
}