#include "display_control.h"
#include "config.h"

// configurate LCD I2C
LiquidCrystal_I2C lcd(0x3f, 20, 4);

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Functions #################################################
//----------------------------------------------------------------------------------------------------------------------------------

void display_setup(){
    lcd.begin(4, 5);
    lcd.backlight();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(0, 2);
    lcd.print("push button to begin");   
}

void print_display_case_wait_for_init(int8_t max_sensors, int8_t sensor_counter, int16_t analog_value){
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(0, 1);
    lcd.print("max. sensor nr. :");
    lcd.setCursor(18, 1);
    lcd.print(max_sensors);
    lcd.setCursor(0, 2);
    lcd.print("act. sensor nr. :");
    lcd.setCursor(18, 2);
    lcd.print(sensor_counter-1); 
    lcd.setCursor(0, 3);
    lcd.print("analog value:");
    lcd.setCursor(15, 3);
    lcd.print(analog_value); 
}

void print_display_sensor_init(int8_t sensor_counter, int16_t analog_value){
    lcd.setCursor(18, 2);
    lcd.print(sensor_counter-1);
    lcd.setCursor(17, 3);
    lcd.print("  ");
    lcd.setCursor(15, 3);
    lcd.print(analog_value);
}

void print_display_init_rdy(int8_t sensor_counter/*, int8_t temp_counter*/){
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(0, 2);
    lcd.print(sensor_counter-1);              
    lcd.setCursor(2, 2);
    lcd.print("sensors recognized");
    delay(2000);
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(2, 1);
    lcd.print("increase to max.");  
    lcd.setCursor(8, 2);
    lcd.print("speed!");
    /*lcd.setCursor(0, 3);
    lcd.print(sensor_counter-1 - temp_counter);
    lcd.setCursor(4, 3);
    lcd.print("sections left");*/
}

void print_display_wait_for_start(){
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(4, 2);
    lcd.print("wait for car");  
    lcd.setCursor(3, 3);
    lcd.print("reaching start");
}

void print_display_wait_for_race(){
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(3, 2);
    lcd.print("push button to");  
    lcd.setCursor(3, 3);
    lcd.print("start the race");
}

int8_t print_display_start_the_race(){		

    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(5, 2);
    lcd.print("Countdown");

    for(int16_t i=3; i>0; i--){     // Count down 3-1 for start signal (buzzer)
        lcd.setCursor(9, 3);
        lcd.print(i);
        digitalWrite(buzzer_pin, HIGH);
        delay(500);
        
        lcd.setCursor(9, 3);
        lcd.print(" ");
        digitalWrite(buzzer_pin, LOW);
        delay(500);
    }

    lcd.setCursor(9, 3);
    lcd.print("GO");
    digitalWrite(buzzer_pin, HIGH);

	return 1;
}

void print_display_race(uint8_t lap_counter, uint32_t time_lap, uint8_t lap_counter_human, uint32_t time_lap_human /*, int16_t section*/)
{
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");

    lcd.setCursor(7, 1);
    lcd.print("Bot");
    lcd.setCursor(14, 1);
    lcd.print("Human");

    lcd.setCursor(0, 2);
    lcd.print("laps:");
    lcd.setCursor(8, 2);
    lcd.print(lap_counter);
    lcd.setCursor(15, 2);
    lcd.print(lap_counter_human);

    lcd.setCursor(0, 3);
    lcd.print("time:");
    lcd.setCursor(6, 3);
    lcd.print(time_lap/1000.00);
    lcd.setCursor(11, 3);
    lcd.print("s");
    lcd.setCursor(13, 3);
    lcd.print(time_lap_human/1000.00);
    lcd.setCursor(18, 3);
    lcd.print("s");
/*  lcd.setCursor(0, 3);
    lcd.print("section:");
    lcd.setCursor(9, 3);
    lcd.print("1");*/
}

void print_display_update_race(uint8_t lap_counter, uint32_t time_lap, uint8_t lap_counter_human, uint32_t time_lap_human /*, int16_t section*/)
{
    lcd.setCursor(8, 2);
    lcd.print(lap_counter);
    lcd.setCursor(15, 2);
    lcd.print(lap_counter_human);

    lcd.setCursor(6, 3);
    lcd.print(time_lap/1000.00);
    lcd.setCursor(13, 3);
    lcd.print(time_lap_human/1000.00);
/*    lcd.setCursor(9, 3);
    lcd.print(section);*/
}

void print_display_finish_race(uint8_t lap_counter, uint8_t lap_counter_human)
{
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(8, 1);
    lcd.print("Finish");
    lcd.setCursor(4, 2);
    lcd.print("the winner is");
    
    if (lap_counter_human > lap_counter){
        lcd.setCursor(8, 3);
        lcd.print("Human");
    }
    else {
        lcd.setCursor(6, 3);
        lcd.print("CarreraBot");
    }

}

void print_display_wait_after_reset(){
    lcd.clear();
    lcd.setCursor(5, 0);
    lcd.print("CarreraBot");
    lcd.setCursor(0, 2);
    lcd.print("push button to begin");
	lcd.setCursor(1, 3);
    lcd.print("speed determination");  
}