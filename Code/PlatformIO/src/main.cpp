#include <Arduino.h>
#include "wifi_control.h"
#include "server_control.h"
#include "display_control.h"
#include "pin_control.h"
#include "state_machine.h"
#include "config.h"

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Defined Variables #################################################
//----------------------------------------------------------------------------------------------------------------------------------

// bool variables
bool restart_code = LOW;                     // flag to restart race by pushing start button for 2s

// uint variables
uint32_t time1 = 0;
uint32_t time2 = 0;

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Functions #################################################
//----------------------------------------------------------------------------------------------------------------------------------

void setup()
{
  Serial.begin(115200);
  Serial.println();
  Serial.println();
  wifi_setup();										    	// setup for the wifi
  //wifi_sleep();												// switch off wifi
  display_setup();											// setup for the display
  server_setup();										  	// setup for the web server
  state_machine_setup();								// setup for the state machine
  pin_setup();											  	// setup fot the pin´s
}

void loop() 
{
  server_handler();								      // handler for the web server
  state_machine_handler();							// handler for the state machine
  
  // restart programm after pushing start 2s
  if (digitalRead(button_pin) == HIGH){
    //Serial.println("Restart by pushing start");
    if (time1 == 0){
      time1 = millis();
    }
    time2 = millis() - time1;
    //Serial.println(time2);
    if (time2 >= 2000){
      Serial.println("Restart after 2s of pushing start button");
      restart_code = HIGH;
    } 
  } 
  else {
    time1 = 0;
    time2 = 0;
  }
  
  // reset all values
  if ((webserver_button_restart == HIGH) || (restart_code == HIGH)) {
    // short beep for restart
    digitalWrite(buzzer_pin, HIGH);
    delay(100);
    digitalWrite(buzzer_pin, LOW);
    
    state_machine_reset();
    time1 = 0;          // time variable for reset by pushing 2s start
    time2 = 0;          // time variable for reset by pushing 2s start  
    webserver_button_start = LOW;      // flag to start race via webserver
    webserver_button_restart = LOW;    // flag to restart race via webserver
    restart_code = LOW;                // flag to restart race by pushing start button for 2s
  }
}






