#include "state_machine.h"

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Defined Variables #################################################
//----------------------------------------------------------------------------------------------------------------------------------

#define pwm_initial_signal 165      		// signal for the car to power the uC
#define pwm_start_signal 300        		// signal for the initial lap
#define pwm_race_signal 600         		// signal for the race
#define pwm_brake 90               			// signal to reduce the speed before reaching start
#define max_sensors 16						// max. number of hall-sensors
#define threshold 10						// threshold for detecting the hall-sensors
#define car_control_increase_speed 30       // increase the speed by controlling the car
#define car_control_decrease_speed_hard 150 // decrease the speed hard by controlling the car
#define car_control_decrease_speed_soft 100 // decrease the speed soft by controlling the car9
#define max_speed 1023						// the max. analog-value/speed that the car can reach in a section
#define human_sensor_value 500				// fix hall-sensor value for the human driver. the right connector is marked on the housing
#define lap_number_for_win 5				// number of laps to win 

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# States for the State-Machine #################################################
//----------------------------------------------------------------------------------------------------------------------------------

enum states {
	wait_for_init,							// the program wait for the start-button
	sensor_init,							// car drive on round and store all sensor-values in the positions-array
	increase_to_max_speed,					// Car increase speed to the max on each section 
	wait_for_start,							// car drives to start and wait for the start-button
	start_the_race,							// wait for start
	control_the_car,						// controlling the CarreraBot	
	wait_after_reset						// wait for start_button to start increase to max speed
};	

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Variable Declaration #################################################
//----------------------------------------------------------------------------------------------------------------------------------
// int variables
int8_t start_counter = 0;					// contain the number how often the car drives over start
int16_t analog_value = 0;            	   	// contains current sensor value
int16_t analog_value_old = 0;         	  	// contains old sensor value
int8_t time_flag = 0;                	  	// time flag for section time
int8_t time_flag_human = 0;          	  	// human time flag for section time
int8_t temp_counter = 0;					// count array value over the length
int8_t buzzer_off = 0;						// reset the buzzer after the start signal

int16_t sections_speed[16] = {0};           // speed values to control the car on the track
int16_t sections_speed_old[16] = {0};		// contains analog values from the last round
int8_t sections_speed_flag[16] = {0};      // flags to recognize of car on on or off the track	|	0:default; 1:Car on Track; 2: Car off the track; 3: no speed change
int8_t sections_flag[16] = {0};				// flags to recognize what |	0:default; 1: 1st time -> reduce speed hard; 2: 2nd time --> reduce speed soft on section before;
int8_t sections_speed_counter = 0;			// Counts the laps with no analog-value change. If there are enough the program will change the state	

// uint variables
uint8_t sensor_counter = 1;             	// counter of sensors
uint8_t state = wait_for_init;          	// initialize case

uint16_t positions[17] = {0};           	// array of sensor values [0] default value [1] ... [16] --> 16 sensor positions

uint8_t start_flag = 0;                		// flag for one round
uint8_t lap_counter = 0;               		// counter of laps

// time variables CarreraBot
uint32_t time_start = 0;                	// time at the start of one round
uint32_t time_lap = 0;                  	// time for one round
uint32_t time_lap_longest = 0;          	// longest lap
uint32_t time_lap_shortest = 0;         	// shortest lap

uint8_t start_flag_human = 0;         		// human flag for one round               
uint8_t lap_counter_human = 0;         		// human counter of laps  

// time variables human
uint32_t time_start_human = 0;        		// human time at the start of one round     
uint32_t time_lap_human = 0;           		// human time for one round                  
uint32_t time_lap_longest_human = 0;   		// human longest lap                      
uint32_t time_lap_shortest_human = 0;		// human shortest lap

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Functions #################################################
//----------------------------------------------------------------------------------------------------------------------------------

// set race speed on default for each section
void state_machine_setup()
{
	// reset speed sections
	for (int16_t i = 1; i <= max_sensors; i++){
		sections_speed[i] = pwm_race_signal;	
		//Serial.println(i);Serial.println(sections_speed[i]);
	}
}

// pressing 2s the start-button will reset the program but not the sensor initialization!!!
void state_machine_reset()
{
	//print LCD
	print_display_wait_after_reset();

	// reset speed sections
	for (int16_t i = 1; i <= max_sensors; i++){
		sections_speed[i] = pwm_race_signal;
		sections_speed_old[1] = 0;
		sections_speed_flag[1] = 0; 
		sections_flag[i-1] = 0;	
		//Serial.println(i);Serial.println(sections_speed[i]);
	}

	//reset variables
    state = wait_after_reset;    				// wait until starting the speed determination
    lap_counter = 0;               				// counter of laps
    lap_counter_human = 0;         				// counter of laps    
    analog_value = 0;              				// contains current sensor value
    analog_value_old = 0;          				// contains old sensor value
    start_flag = 0;                				// flag for one round
    start_flag_human = 0;						// human flag for one round

    start_counter = 0;                	    	// flag for one round
	
	// set pwm signal to supply the uC on the car
    analogWrite(pwm_pin, pwm_initial_signal);         

    // times for Bot
    time_start = 0;      						// time at the start of one round
    time_lap = 0;        						// time for one round
    time_lap_longest = 0;   					// longest lap
    time_lap_shortest = 0;  					// shortest lap

    // times for human
    time_start_human = 0;      					// time at the start of one round
    time_lap_human = 0;        					// time for one round
    time_lap_longest_human = 0;   				// longest lap
    time_lap_shortest_human = 0;  				// shortest lap

    delay(1000);
}

// function for the state machine
void state_machine_handler ()
{
    switch (state){
        //----------------------------------------------------------------------------------------------------------------------------------
        //################################################# Wait for sensor initialization #################################################
        //----------------------------------------------------------------------------------------------------------------------------------
        case wait_for_init:
            
            // waiting for button 
            if ((digitalRead(button_pin) == HIGH) || (webserver_button_start == HIGH)){
                
                // read analog value as default // the car shouldn´t stand on a sensor to read the right default value
                positions[0] = analogRead(analog_pin);                                    
                Serial.print("standard value for no sensor detected: ");Serial.println(positions[0]);
                
				// set new state
                state = sensor_init;
				//Serial.print("Curent Case: ");Serial.println(state);
				
                // reset web server button value
                webserver_button_start = LOW;
                //Serial.print("web server button value: ");Serial.println(webserver_button_start);                

				// print LCD
                print_display_case_wait_for_init(max_sensors, sensor_counter, analog_value);
                delay(1000);
				
				// set pwm value for the first round  
				analogWrite(pwm_pin, pwm_start_signal);
            }
            break;

        //----------------------------------------------------------------------------------------------------------------------    
        //################################################# Initialize sensors #################################################   
        //----------------------------------------------------------------------------------------------------------------------
        case sensor_init:    
            // detected the number and values of sensors
            
			// read analog value
            analog_value = analogRead(analog_pin);
			
            //Serial.println("");
            //Serial.print("analog value: ");Serial.println(analog_value);
            //Serial.print("analog value old: ");Serial.println(analog_value_old);

			// print LCD			
            print_display_sensor_init(sensor_counter, analog_value);

			// look for the hall-sensor values 
            if ((analog_value < positions[0] - threshold)&&((analog_value < analog_value_old - threshold) || (analog_value > analog_value_old + threshold))) { // x - 5 ... x  //  every sensor value should be lower than 1023
				Serial.print("Analog value: ");Serial.println(analog_value);

				for (int16_t i = 1; i <= max_sensors+1; i++) {
					Serial.print("Array Counter: ");Serial.println(i);      // attention!! if this line will be removed the code is not working

					if (((analog_value >= positions[i] - threshold) && (analog_value <= positions[i] + threshold)) && positions[i] > 0) {   
						// If one sensor value measured twice -> Start, round complete
						//Serial.println("Start detected");
						
						// print LCD
						print_display_init_rdy(sensor_counter/*, temp_counter*/);
				
						// set new state
						state = increase_to_max_speed;
						//Serial.print("Curent Case: ");Serial.println(state);
						
						// print all analog values for the positions 
						for (int16_t i = 1; i <= max_sensors+1; i++) {
							Serial.print("Position: ");Serial.println(i);
							Serial.print("Value: ");Serial.println(positions[i]);
							Serial.print(" ");
						}
						
						// write fix value to positions array for the human driver
						positions[sensor_counter] = human_sensor_value;             	// value for human car sensor should be the last sensor socket

						// write pwm signal
						analogWrite(pwm_pin, pwm_race_signal);

						goto start_detected;                                			// no great solution!
					}
				}
				// write current analog value to variable
				positions[sensor_counter] = analog_value;
				Serial.print("value of actual detected sensor: ");Serial.println(positions[sensor_counter]);
				
				// increment number of sensors
				sensor_counter = sensor_counter + 1;
				Serial.print("Number of sensors: ");Serial.println(sensor_counter - 1);


                analog_value_old = analog_value;
            }
                        
			// jump marker
            start_detected:

            break;

        //-------------------------------------------------------------------------------------------------------------------------------------------
        //#################################################  Determine speed value for the sections #################################################
        //-------------------------------------------------------------------------------------------------------------------------------------------
       case increase_to_max_speed:
            // read current analog value
            analog_value = analogRead(analog_pin);																					
            
            // if the car is off the track push the start button to decrease the speed value at this section
            if ((digitalRead(button_pin) == HIGH) || (webserver_button_start == HIGH)) {											
                for (int16_t i = 1; i <= sensor_counter - 1; i++){
                    //Serial.print("look for array value: ");Serial.println(i);
                    if (sections_speed_flag[i] == 1) {																				
                        // car is off the track
                        sections_speed_flag[i] = 2;																					
                        Serial.print("Car off the track; flag: ");Serial.println(sections_speed_flag[i]);	
						
                        break;
                    }
                }
            }
			
            // example analog_value_old = XXX
			// if ((900 < 1020 - 10)  &&  ((900 < XXX - 10) || (900 > XXX + 10)))
            if ((analog_value < positions[0] - threshold)&&((analog_value < analog_value_old - threshold) || (analog_value > analog_value_old + threshold))) { // x - 5 ... x  //  every sensor value should be lower than 1023
				for (int16_t i = 1; i <= sensor_counter-1; i++) {     
				
					// if (((900 >= YYY - 10) && (900 <= YYY + 10))		 && YYY > 0)
					if (((analog_value >= positions[i] - threshold) && (analog_value <= positions[i] + threshold)) && positions[i] > 0) {    
						
						// update race speed after each detected sensor
						analogWrite(pwm_pin, sections_speed[i]);

						Serial.print("Counter: ");Serial.print(i);
						Serial.print(" | Currrent speed: ");Serial.println(sections_speed[i]);

						// sections_speed [0] = Start - section 1, [1] = section 1 - section 2, ... 
						if (sections_speed_flag[i] == 0){     // 0: nothing; 1: car is on section;
							sections_speed_flag[i] = 1;
							//Serial.print("section: ");Serial.println(i);
							//Serial.print("Flag car is on track: ");Serial.println(sections_speed_flag[i]);     
						}
						
						// if the section speed flag of the sensor before is 2 decreasing the speed
						if (sections_speed_flag[i-1] == 2){
							
							sections_speed_flag[i-1] = 0;			// reset value to 0														
							
							if ((sections_flag[i-1] == 0) || (sections_flag[i-1] == 2)){
								sections_speed[i-1] = sections_speed[i-1] - car_control_decrease_speed_hard;
								//Serial.print("Decreased speed value: ");Serial.println(sections_speed[i-1]);
																						
							}

							// if i - 2 is lower than 0; if this is true the code have to work with the sensor_counter variable
							
							if ((i-2) == 0){                            // ((i-2) < 0) 
											
								//Serial.print("Decreased speed value: ");Serial.println(sections_speed[i-2]);                                      

								// only decrease speed by the 2nd time off the track for the section before
								if ((sections_flag[i-1] == 0) || (sections_flag[i-1] == 2)){      					
									sections_flag[i-1] = 1;
								}
								else if (sections_flag[i-1] == 1) {
									sections_speed[sensor_counter - 1] = sections_speed[sensor_counter - 1] - car_control_decrease_speed_soft;
									sections_flag[sensor_counter-1] = 2;
									sections_flag[i-1] = 2;
								}    
							}      
							
							else {                                     																			
								// only decrease speed by the 2nd time off the track for the section before
								if ((sections_flag[i-1] == 0) || (sections_flag[i-1] == 2)){                        
									sections_flag[i-1] = 1;
								}
								
								else if (sections_flag[i-1] == 1) {
									sections_speed[i-2] = sections_speed[i-2] - car_control_decrease_speed_soft;
									sections_flag[i-2] = 2;
									sections_flag[i-1] = 2;
								}
							}                                           																			
							break;
						}
                        // if the section speed flag of the sensor before is 1 increase the speed
						else if ((sections_speed_flag[i-1] == 1) && (sections_flag[i-1] == 0)){
							//Serial.print("section: ");Serial.println(i-1);
							sections_speed[i-1] = sections_speed[i-1] + car_control_increase_speed;
							//Serial.print("Increased speed value: ");Serial.println(sections_speed[i-1]);
							
							sections_speed_flag[i-1] = 0;
							
							// set max speed
							if (sections_speed[i-1] >= 1020){
								sections_flag[i-1] = 3;
								sections_speed[i-1] = max_speed;
								//Serial.print("Reached max speed: ");Serial.println(sections_speed[i-1]);
							}
							//Serial.print("Flag car is on track: ");Serial.println(sections_speed_flag[i-1]);
							break;
						}

						// special for the section before start
						else if (i-1 == 0){
							// if the section speed flag of the sensor before is 2 decreasing the speed
							if (sections_speed_flag[sensor_counter-1] == 2){
								//Serial.print("section: ");Serial.println(sensor_counter-1);
								if ((sections_flag[sensor_counter-1] == 0) || (sections_flag[sensor_counter-1] == 2)){
									sections_speed[sensor_counter-1] = sections_speed[sensor_counter-1] - car_control_decrease_speed_hard;
								}       
								// reset flag to default
								sections_speed_flag[sensor_counter-1] = 0;
								
								if ((sections_flag[sensor_counter-1] == 0) || (sections_flag[sensor_counter-1] == 2)) {
									sections_flag[sensor_counter-1] = 1;
								}
								else if (sections_flag[sensor_counter-1] == 1){
									sections_speed[sensor_counter-2] = sections_speed[sensor_counter-2] - car_control_decrease_speed_soft;									
    								sections_flag[sensor_counter-2] = 2;
    								sections_flag[sensor_counter-1] = 2;
								}													
								break;
							}
                            // if the section speed flag of the sensor before is 1 increasing the speed
							else if ((sections_speed_flag[sensor_counter-1] == 1) && (sections_flag[sensor_counter-1] == 0)){
								//Serial.print("section: ");Serial.println(sensor_counter-1);
								sections_speed[sensor_counter-1] = sections_speed[sensor_counter-1] + car_control_increase_speed;
								//Serial.print("Increased speed value: ");Serial.println(sections_speed[sensor_counter-1]);
								if (sections_speed[sensor_counter-1] >= 1020){
									sections_flag[sensor_counter-1] = 3;
									sections_speed[sensor_counter-1] = max_speed;
									//Serial.print("Reached max speed: ");Serial.println(sections_speed_flag[sensor_counter-1]);
								}				
								else {
									sections_speed_flag[sensor_counter-1] = 0;
								}						
								break;
							}
						}		
						
						for (int16_t i = 1; i <= sensor_counter-1; i++) {
							// if speed and speed_old is the same value increment the counter 
							if (sections_speed_old[i] == sections_speed[i]){
								temp_counter = temp_counter + 1;
								//Serial.print("temp_counter: ");Serial.println(temp_counter);
							}
							sections_speed_old[i] = sections_speed[i];
						}
						// if temp_counter == the number of sensors increment the speed_counter
						if (temp_counter == sensor_counter - 1) {
							sections_speed_counter = sections_speed_counter + 1;
							Serial.print("sections_speed_counter: ");Serial.println(sections_speed_counter);
						}
						else {
							// reset the counter
							sections_speed_counter = 0;
						}

						if (sections_speed_counter == 20) {
							for (int16_t i = 1; i<=sensor_counter - 1; i++){
								Serial.print("section: ");Serial.println(i);
								//Serial.print("speedflag array: ");Serial.println(sections_speed_flag[i]);
								Serial.print("speedflag array: ");Serial.println(sections_speed[i]);
							}
							// car speed on min to drive to start
							analogWrite(pwm_pin, pwm_start_signal);

							// set new state
							state = wait_for_start;
							//Serial.print("Curent Case: ");Serial.println(state);

							print_display_wait_for_start();
						}
						else {
							temp_counter = 0;
						}
					}  
					// write current analog value to analog value old  
					analog_value_old = analog_value;					
				}
				
			// write current analog value to analog value old      
			// analog_value_old = analog_value;  
            }    
            break;

        //------------------------------------------------------------------------------------------------------------------
        //################################################# Drive car to start #################################################
        //------------------------------------------------------------------------------------------------------------------
        case wait_for_start:
            // read current analog value
            analog_value = analogRead(analog_pin);

            //Serial.print("Currrent speed: ");Serial.println(sections_speed[i]);
            //Serial.println("Drive car to start");
            
            // car reduce his speed in 2 laps to stop at start
            if (((analog_value >= positions[1] - threshold) && (analog_value <= positions[1] + threshold)) && positions[1] > 0) {
                //Serial.println("Car reached start");
                
				// reduce speed to stop at start
                if (start_counter == 0){
                    analogWrite(pwm_pin, (pwm_start_signal - (pwm_brake)));
                    //Serial.print("Current speed: ");Serial.println(pwm_start_signal - pwm_brake);
                    //Serial.print("start_counter: ");Serial.println(start_counter);
					
					// set new state
                    state = start_the_race;

					//increment start counter
                    start_counter = start_counter + 1;

                    delay(500);    
                    
                    // print LCD
                    print_display_wait_for_race();                 
                }
            }

            break;
        //-------------------------------------------------------------------------------------------------------------------------------    
        //################################################# Wait for start #################################################
        //-------------------------------------------------------------------------------------------------------------------------------
        case start_the_race:
            // read current analog value         
            analog_value = analogRead(analog_pin);

            if (((analog_value >= positions[1] - threshold) && (analog_value <= positions[1] + threshold)) && positions[1] > 0) {
                
                if ((digitalRead(button_pin) == HIGH) || (webserver_button_start == HIGH)) { 
					// print LCD and return flag to reset the buzzer
                    buzzer_off = print_display_start_the_race();
					
					// reset webserver_button start
                    webserver_button_start = LOW;

					// set new state
                    state = control_the_car;
                    //Serial.println("Curent Case");Serial.println(state);
                }
            }        
			break;
        //----------------------------------------------------------------------------------------------------------------------    
        //################################################# Control the Car and Determine position ################################################# 
        //----------------------------------------------------------------------------------------------------------------------     
        case control_the_car: 
            // read current analog value
            analog_value = analogRead(analog_pin);

            // turn buzzer off
            if (buzzer_off == 1) {
                
                // print LCD
                print_display_race(lap_counter, time_lap, lap_counter_human, time_lap_human);

                delay(500);
				// reset buzzer value
                digitalWrite(buzzer_pin, LOW);

                // start with max speed
                analogWrite(pwm_pin, 1023);
				
				// reset buzzer variable
				buzzer_off = 0;
				
				// reset lap counter
                lap_counter_human = 0;
                lap_counter = 0;
            }

			// look for analog value change     
            if ((analog_value < positions[0] - threshold)&&((analog_value < analog_value_old - threshold) || (analog_value > analog_value_old + threshold))) {            // x - threshold ... x  // normally every sensor value should be lower than 790    
                for (int16_t i = 1; i <= sensor_counter-1; i++) {
                    if (((analog_value >= positions[i] - threshold) && (analog_value <= positions[i] + threshold)) && positions[i] > 0) {                   

                        //Serial.print("Lap Time:");Serial.print(time_lap);

                        // set current section speed
                        analogWrite(pwm_pin, sections_speed[i]);
                        //Serial.print("Counter: ");Serial.print(i);
                        //Serial.print(" | Currrent speed: ");Serial.println(sections_speed[i]);					

                        if (i == 1) {       											
                            //Serial.println("start flag");Serial.println(start_flag);
                            // save the start time
                            if (start_flag == 0){
                                time_start = millis();
                                start_flag = 1;
                            //Serial.println("time at start");Serial.println(time_start); 
                            }
                            // if one round is complete 
                            else if (start_flag == 2){
                                time_lap = (millis() - time_start);
                                time_start = millis(); 
                            // detect longest and shortest lap
                            if (time_lap > time_lap_longest){
                                time_lap_longest = time_lap;
                            }
                            else if ((time_lap < time_lap_shortest) || (time_lap_shortest == 0)){
                                time_lap_shortest = time_lap;
                            }
                            // set flag
                            start_flag = 1; 
                            lap_counter = lap_counter + 1;
                            print_display_update_race(lap_counter, time_lap, lap_counter_human, time_lap_human);
                            }
                        } 
                    break;
                    } 
                }
                // recognize human car with fixed value in positions[sensor_counter]
                if (((analog_value >= positions[sensor_counter] - threshold) && (analog_value <= positions[sensor_counter] + threshold)) && positions[sensor_counter] > 0) {
                    // get lap time, shortest lap, longest lap, lap counter
					
                    //Serial.println("start flag");Serial.println(start_flag);
                    //Serial.print("human analog value:");Serial.println(analog_value);

                    // save the start time
                    if (start_flag_human == 0){
                        time_start_human = millis();
                        start_flag_human = 1;
                    //Serial.println("time at start");Serial.println(time_start); 
                    }
                    // if one round is complete 
                    else if (start_flag_human == 2){
                        time_lap_human = (millis() - time_start_human);
                        time_start_human = millis(); 
                        // detect longest and shortest lap
                        if (time_lap_human > time_lap_longest_human){
                            time_lap_longest_human = time_lap_human;
                        }
                        else if ((time_lap_human < time_lap_shortest_human) || (time_lap_shortest_human == 0)){
                            time_lap_shortest_human = time_lap_human;
                        }
                        // set flag
                        start_flag_human = 1; 
                        lap_counter_human = lap_counter_human + 1;
                        // print LCD
                        print_display_update_race(lap_counter, time_lap, lap_counter_human, time_lap_human);
                    }
                    //Serial.print("lap_counter_human: ");Serial.println(lap_counter_human);
                    //Serial.print("time_lap_human: ");Serial.println(time_lap_human);
                    //Serial.print("time_lap_shortest_human: ");Serial.println(time_lap_shortest_human);
                    //Serial.print("time_lap_longest_human: ");Serial.println(time_lap_longest_human);  
                }              

                // write current value as old value
                analog_value_old = analog_value;
            }  
            // check for changed position carreraBot
            if (start_flag == 1){
                start_flag = 2;
            }
            // check for changed position human
            if (start_flag_human == 1){
                start_flag_human = 2;                     
            }
            // Quit race after X laps
            if ((lap_counter == lap_number_for_win) || (lap_counter_human == lap_number_for_win)){

                // car speed on min to drive to start
				analogWrite(pwm_pin, pwm_start_signal);

                print_display_finish_race(lap_counter, lap_counter_human);

				// set new state
                state = wait_for_start;
				
				// reset variables
                lap_counter = 0;
                lap_counter_human = 0;
                start_flag = 0;
                start_flag_human = 0;
                time_lap = 0.00;
                time_lap_human = 0.00;

                // reset for state wait_for_start
                start_counter = 0;
				
				// reset old analog value
                analog_value_old = 0;
            }   
			break;
		//-------------------------------------------------------------------------------------------------------------------------------    
        //################################################# Wait after reset #################################################
        //-------------------------------------------------------------------------------------------------------------------------------
        case wait_after_reset:
			// waiting for button 
            if ((digitalRead(button_pin) == HIGH) || (webserver_button_start == HIGH)){
                
				// print LCD
				print_display_init_rdy(sensor_counter/*, temp_counter*/);
				
				// set new state
                state = increase_to_max_speed;
				//Serial.print("Curent Case: ");Serial.println(state);
				
                // reset web server button value
                webserver_button_start = LOW;
                //Serial.print("web server button value: ");Serial.println(webserver_button_start);                
				
				// set pwm value for the first round  
				analogWrite(pwm_pin, pwm_start_signal);
            }			
		
			break;
    }
}