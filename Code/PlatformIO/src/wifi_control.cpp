#include "wifi_control.h"

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Defined Variables #################################################
//----------------------------------------------------------------------------------------------------------------------------------

// ESP IP: 192.168.4.1
char ssid[] = "CarreraBot_WLAN";     // your network SSID (name)
char pass[] = "CarreraBot";          // your network password
bool WiFiAP = true;

//----------------------------------------------------------------------------------------------------------------------------------
//################################################# Functions #################################################
//----------------------------------------------------------------------------------------------------------------------------------

// start wifi client
void startWiFiClient()
{
	Serial.println("Connecting to "+(String)ssid);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, pass);

	// wait until connected
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: " + WiFi.localIP().toString());
}

// start the wifi AP
void startWiFiAP()
{
	WiFi.mode(WIFI_AP);
	WiFi.softAP(ssid, pass);
	Serial.println("AP started");
	Serial.println("IP address: " + WiFi.softAPIP().toString());
}

// set up up your wifi as client or AP
void wifi_setup()
{
	if (WiFiAP)
		startWiFiAP();
	else
		startWiFiClient();
}

// turn the wifi off
void wifi_sleep()
{
	WiFi.mode( WIFI_OFF );
	WiFi.forceSleepBegin();
}

