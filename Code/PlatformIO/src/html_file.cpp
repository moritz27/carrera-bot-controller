#include "html_file.h"

char webpage[] PROGMEM = R"=====(
<!DOCTYPE HTML>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="refresh" content="60";url=http://192.168.4.1/>
		<!--the logo of the university in an converted format to store it on the microcontroller-->
		<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXIAAABCCAYAAABKKV0QAAAaSklEQVR4nO1dT2gUSfsu8aQwJ2HBOWxuriS4Mpm5hBAwh0yc6ulL2DCIFwniJfAFJHiQzUXCkoPE76KBsMSFHxo8CDmJnwcFQUFQk+7MZDKZvxES0FX8QXC/gIf3O1RV/6mu/jfTM3GSeqBY3FT3VFdVP/X2U+/7FkJHEBsnk6frqD9XQ8nbNZS8XUeJBzWUKtWPpcBaaihVqqLE9XGEjh90myUkJCSOLMYHBk400PnhKkpcp6T9hidsz4ISb4ro7KmDfg4JCQmJQ4siOnuqgc4P11F/bnxg4ARCCDXQ+WFC2qkndZT8JyRxP6iivp+L6OwpaYlLSEhItBGMrE3CTrypo9STGur/FIq4HUSeeiItcAkJCYk2oYjOnqqixHWbPBLW4g5YGuj88EE/r4SEhMShQRX1/VxHiQdecgixzlu0xjnL/KCfW0JCQqLrUUHnksLNSZR6UkWJ61XU93MN/foLsdKjt8xr6NdfDroPJCQkJLoSxDXQtK5rqP8T0cLNDc0iOnuqhpK3rXVCe6T4lCpKXD/ovpCQkJDoKtRRf85KzMzqZn8fR+g4IXnq840Sb2ooeZu5Fop8wSWRS0hISHQAhMCT0EB9hvufqJ4po/TnKuhckrgdJh60a8OzjhIPOt0XEhISEl2FOurPNdAZ+IBiUEeJB0w28YJplUe4sSktcgkJCYlwYJuY26gHttR//WfjZPK03zWG9d1m8j7MJB5koZSQOAiMI3RcBuBFDO39+z+0gv7/en69EFVZ/fa5oOfXC5Vzv336gGJQ7R2DwsMVWP329721nfr71W+fC2s79fessH9v3P0/rXLut08N1Af1Y8k2yCfJf2ooVSIlebuGfv2l0wFBGOOeTCZ7A+PsFMbZ3zMZNe1WD+Ps7371rMhkMgmM1fuKopYxzn4n/1Xvp9Pq2WbbijG+lMlkb5A240v8vRRFGSPtI3VGRkbionsFrceQTqd/ymTUNMbZqUwme0NRlLF0Ov1TmPan0+pZjPEV1t+kDbiHe8YrrF2KolwbHByMubT/Gmu7oijX+IVyHKHj1joY4ysismLjT+6j3hKNq6Iow4qi3mLPHnT82O/TOeMolrb1+N/NG+MIHc9kMglFUa6xeZrJqGm3/kMIIYzxJYzV54qifsU4+1+MVQ3j7O9e1yBE5sLFi9kBRVHG+HEQjamorVGNzeDgYCyTySQyGTVtv6d4foXByMhI3DrnMcZXLl7MDgQyyrTC+p96YR1aKnkd9MI6aJ93QPu8A8XZBdhGPbCNemDz5h3QPu/A2v4X0MuboOsa6HkdtEYF1vY+gq5rUJxdgGrvGGyjHmigMxGSN7cY/AC5VOhLCqxgnBX6sJOJ4l+PAePs79b6zqJcC9pGQoDZJ273sk5W+mJaf0cYXKUo2ddB6o2MjMTpYuT2LLf82q8oyjDGqiZuu7prfYkVRf3q9mwMhAjUPa96g4ODMe63vooIipCRrT2P+DoYK3e537ri98yiNnqM3yW/+3mBLH7qrkv/Op6H9A0//ray57VY+VzLfldzm1PjAwMn+LERGQWCsXnsrKPOBejjsqIoY0H7kxhgXu+b+7MZaJnI8zpouw1Y2/8C+ZWnUO0dA2aF51++grX9L6A1Kk4CLxZg8+YdqMVHYRvFYRv1tGCFJ43SQH3QQGfootAHNZQqkbD+1JM6SjwIIu20E04id04WhERE7nxBGPxJPPgLTK0Rr/vYCIon8osXswPi57a/jKJ6dEJ/93lhhf1ltsdOgqKXwt4uk8gxzn4XfSnwJIlx9r88EfBEjrG6K7Kkgoyr8xn8SSEMkYchGeczepMqxsq883nEiypP5m5faQGvZ2Mz5egbjsgxVndFi6xgbBxzzW9+ccXX6Ajy7mKsaplMJuF5o6aJPK+DXt40rOqt3DQl5DixwncboH3eIfV0jZD93kfQC+sWAm/FAjdJm5X6sRTU4qNQViahNDVnaPLWjIiWfC0PDiI0P2oi5+9H697HGF9RFLXMTfL/ekkaAUjc0Y6oiDydTv+kcNYxazNHto4X1WxLoJfM9nIdciL/Si3nr5YCQWQ6HoODg7EghMrfW1HUW/x4UinCQWBuc5z/XYzVx0TOUe+LFn6+DW0m8jLG6iPnl6n/2AVdFDDO/u47QKGJnCPm4vwS1I8lHVY4k1FYvfW376A0NdcygTdQn3l9bAhKEzNQnF+C/MtXkH/2AjYWl2Hz5h0oTcxA5dxvn4i7ot3St3nBoP6cbydFiKiJnJ9AmUz2BvsbJReezIWTQrQgECLAl6hW3kM1StviFxWRi154jPGlwcHB2PjAwAlirSt33SwTunAJLBk1zdpP9H779YeVyEkbiVwxODgYs5ZmNsJFpIOx+piMC+5h+xHW/iNzxn6NVULh+4LMX+f48kRuvYdogeFlyHYSufUrl8wxfrHLvhb3Z3ZK0J+7iqJco33ZQ/Yg1Ftu75QNoYicWeH7X2D97TsoX7gKH1AMtlEcSlNzphWua4aEsv72HZQmZqAeG2qawBl5b6MeqMVHoZK6DKWpOWPRyD97AWVlEmrxUWigPviAYrRdRF4hhJ16wnzSETJT4rJ/dwpRErmAfMuCOmN+dRASWj1akBc+KiJ3aq4+mqAFlEQ5DVu9H+Taw0rkiosO3AxEi7zVYHC/zqEnO6QGwX6IqI5m/2072ZONULsBYiXqThE5QmR/yUnQ9v2UdDr9E/3StP7W85Y8eQITOW+Fx4aIT3hsCAoPVwwr3NDAy5tQmpqLgMDjROuOj0JpYgbyL1+RBcNSNhaXodo7Blu5aSjOLkBxfgk2Fpdh/a/HSwiRRFwHvcnJEC2RO6xYh7VNJ7FNsnBapQ6yF276iRAFkfOTn9ex/SCQhISLlbhdh5PIRW1sFvxGnNd+jf06d0uaQdAfjrH3I3JaZ9fttzpJ5LQt3FeyXerhZSW39oRCICLXNeKRstuA0sQMbKM4fEAxKF+4CnpeN4h7be8jaLsNKM4vtSShMAKvH0vBVm4aCg9XyCJB26AXC/bN1kbF+BpY2/tolNVvf99rqXPagCiJ3DnB3VwZ+RfR7gUhsIrmgj5PFEROPyGbJnL+3kG8PMxrJZF7QSSP+G68IeHivCuqR7yU7Do33+5miNw6jgdA5Pfdxm8coeN8W1v1IkIIBSByXTMkEuaRso16iJRCiZW5HW4sLrdI4GcogScN63tt7yORa8qb3ouNldxp0Qrr/265gyJGVEROtEH755m7D7fdcrcS9fjAwAn+PkFeVIYoiHxkZCTeDFkgJCKM7Pcw1o0kcm849x7Emi+PIG6W5m/YiVogu3kSud8X3Q9A5IZMKJCpvkYSHOVJ5NS1MP/ylSGlNFAfbCwuE7dCKrXkn72A8oWrho4d1o3QsMBjQ6b2zQjc4qcetnQJkQsnuN8LL7BihRYPQs6X0TpB+fsoiloOsxkWoUb+nGtjII3eSRje/vbO621ELvTq6UYiVyLSyDFW5rn54etSh5BQ7nK9DmP1sRc5+hG5n999J4lcZHFbx4EEMQVb4ELBk8jLm6CXNw1LvIH6DD2cySibN+9YpJCwfuBJY0OyNDFDrH+6QLRC4F1G5MZkGR8YOMFefn8i99cWLb/JaeCmVdUqETrdroj1wZ6FWRt+RC7yYFAUtewX1cgThsiP2QsckX9newPWsaBE0FVEbl2UjLGwjEdQCGS5QDIAvwB4bY7yFizvYupG5NRL5L5ff3aSyAVEfd/7end32lDwInIml5ANxzMkzB72bcE/7G/NySg9UL5wFfLPXhgLQxQE3k1Ezl46vjjr8P7b+JLfpHP7Tav1zrtBBfX2MK93+s8GeR6R5e7mV+tNAvZrgnhT2PuGj+zMfvdrfzcQuehZ2IIfhswVzn01qA+6n5XN/Yand4vAvfC7YE7tuUUvt9eP3BwbkQssb4g0uzD6wo/It3LT8AHFYGNxGVZhn2x4Ts0ZLoHNyii1+CgU55cMfV0vFiIj8G4j8iCFf+EFK39TRK546OdBICLyIMVNgsFYfeTy/MLnExDGlTDt54k82Fh0B5G7lK9hpDO+f4LuXfhtsHO/wRO5bQ46fbOF8+O5295Iey1ydZc+q2MeiRY9LwcFKsuwv9uCuLzeb4SQB5HTgJ5q7xgUZxdgFfZBLxaoFh43/bNDWuGGjFIsmIFDERP40SRyd0mkm4ictkmYz0L0jJLICY46kVvGxs0Fty1E7tbHbu67iscXDiFy59crKT6bzK5EXiyQaMmVp7AK+1B4uEJ9wpvRwgmJV3vH7DJKmwi8y4h8D2P1McbZJ2ZRHzk/Jx3SiusGZoDfLJt/433Rw2nMTiLPvhY8z2PexcwvWs1FM3dIJzyRh0kORp+fk1bU587286TUHUROMj+qaRqRO0YiW9V0SGnla5hxM58huITgnIN2MnazYlm0r2Ce2KSZg8i14h5B3Uki1zXDL3xt/wvd0DxDpZSwYfVm+L7WqBArPEIdvNuJvHmvFZ7o3Ada4Ar23PK3wBKNCDyRu1lsQZJm8aD5V/gUA9+tJNqsV4XZLhtR7Yk8PbrVa6XlIBMksoaDJdwKs6kncNe7xv3dM7DIT5tuM5HfoukrrjlJ2GlUOL2z+JgOfIksuvzvhCHyvE5cDj/vgNaoQH7lKZSVSXuoe2g9nG5ovnxlJtFqM4F3GZE35UcuCEt2jWb02hj18mgJgqjcDz3a3uP1gji9VsJt1irSj9wTzvSqwb54lABRx+ZvtOZ+KGqn9cutU5udgusdLsH8ouW2OR/6vTSInFnhNHdJtXfMYoU3I6WQTc2yMmluaOpax0j8sBO5IKeDqwXmDAk25ZMguSG80G4iJ33hTtZBc8m4QRK5NxROvw7q9+z80lPuutf1zlUfhMi9vixFRC4amyDvpp/7Ib+gON1sHXPZLatpE0QuyGbYrBZubmqegeLsAgmfb1Q6aokfBSIXBR64ZwbkfW3NzzlRZGcYl6hOELlXn4kWojCRqVEROa1juVdQIncS3I9E5ALZIpBk49yXERMRXQBtOnxzIfr2r07r74nGJphF7j82/LviJGq7pCTY+xFGdjZF5CzU3pRRmj+lh3mmFGcX7IdKdJjEDzuRIxTcY0PhdGZBVCVH9MFznXTGInf0hU0+ceZaCS6vREXkCAVLEsX77Ys+rX8kIhelTwgSxCLKoSImT/88OwGlFT4egvPyss+RqMZGEBDEvd/2xSBorpXQRL767e97+ZevLME94bVwq5zSQGcMn/N2+IZLIrfdi/+cfM7X4bV0kW4nyn4YNLCmE0TuvxnmzO0cdFMuYiLn2+logzOASeRrHMlmp3Djthk4PYPEROi8zt8FUQng/hqMyPk6Du8mzlgR5UgJPzb+aWydBKw45apdfqxCE/n6X4+X2HFrrR14nDTklLX9LwdO4keByEVWjzNlpiPARujVITrCC2PlrnWCkTzK/OHFrRM5Ocg3e0OUg1x0ahHfBlGqXkq4U1Z5Y2RkJM4TdbREzi8o9pdPlElQ9Hs/Uq4VhIQb60DkOHu7MMY99mMAebnDvn9B569vwja/Lx2eGEV1/MZG9OURZGwcuVYGBk5YLW5REjfRe6soatk6r0MR+fjAwIka6v/UihXOyjbqMTY22xnkI4ncDt79juYLuSTKQyEiQfP3HMmzbJOMEbHTnz3a7IeYRsrRBUhEzm4nHDm+Kiz31Cxk0Jaj3khfOIma9I8yTNvHuVKKJSBB9KCGsXqf+LezQvqItcMlRJ/FJFiuU58rIYO+aD+5HTq8R8e2TIjY7hfNP7OiZF9nMpkEXRz4/nCL3uUDguaIT3z2hvP+4liI9o2NyLL33vCk7eEXOaNNGCt3nc/sQeRVlLhOSLwVS5xZ432QX3lKcpP/ACR+VIicHnUlPNFcQIKe2qYXGVpJxXpNFEQe5He9+srss/BnjkZJ5KQ/HH7trsVNnghzwK+R5CvE4cth9kDs7RKnT/CaY26BXeH6I0xkp1e63OjHxkWi4Y0r4XsnlgTdigeR11Cq1KolzjY4WcCP1qgcOIE3Q+SR5AQOACeRi0PrnUTuHoJPdTm/UPOA6UczCUVg5VhJzP75LM5+6HxudxezYAtIgANozXu59gVPYnxd0ReLS9Sk8MuG1PU+aZ4+f8uH8lrb0QkiJ23zJh+Rpwe/l+PSH65H+wUk8q9+6RnaMTZi3T94RlFypqy/IeZqxFRR4noUJM5S0ZYmZog2fkAeKq0QeRGdPVVHiTc1lLwdpH4rIESZfU0/ezW3TcWg9RiIfi2SUsgBxGHbSRecOfY5Tje85kSnpNNP/+cYq8/dLBm/eiw6jvwmC41X5tkBzGHaTl5YZYxcn31iab/jIFvSZ9nXNLXAI7fITq7efa82mZq/aEEh0oJX+2k/lPk0AfaUASQVglVaYW30u05pQlqxgnwF4itUAnhOy/1MJnvDffyVYREhE+nHO26B/Jb6yPk8yl22txLUEItybEhfOhcg+i5SGYvMF6/8NqRNalrwvt0iz457hM9XQeeS0ZC4KauwPCrdZpE30Plh9iydIPJ2Y3BwMEb0R2U4TGCPRPQYHxg4kU6rZ2m+k2G3E5yOCshig3sURRm+eDE7EEUKgabb0u1jEy2J26UVXdc6T+R5nZznudtwbLL6EXkNJW+bJN7/aeNk8nSnxkFCQkKiaVjJK6rC8qkYp/sIzs+MnLzpocvs4Ofi7AIU55dsZ3u6EfnGyeTpOkq8sT0H6s9Z6xTR2VOS2CUkJH44EAJL/hM1kTMf8q3ctHGifVs8V/I66MUCSSew/wXW376D0tQcbOWmoTi7APmXr0wiz+sOIh8fGDhRR4kHfPt5SaWBzg/XUPL2xsnk6Y2TydNh8jdLSEhItBXRbXCK5RV2rmdpYoZILFFZ5lQ+YQcy51++gq3cNNTio7CVmybavNV/nS4iq9/+vseevY76czXU/8luhSceWK3uCjqXrKPEmzpKPKiixPUaSpVqqP9TpzxaJCQkJHwhskaDWtx2X/OkUUiGxDjUY0NQSV2GsjJJjnDz08rLm6b1Tk8jEtah1rfWqMDG4jJUe8egHhsiqXFXnppyjoXAtUaFHEv3/v0fFXQuKSLwKur7mfULqUNdMSmRmwvUeVe3KAkJCYmOo2kijw0ZeVRYYcm1bFYxzaCofd4xiJgVbbcB2ucd47AKrVGB/LMXRBKxeruUN0k9S53S1BxtwxkoK5PklKH9L2Z+cya50OvW376D4uwC1OKjtgRgNZS8XURnTyFEdtCJlW7xpbfo5ofBg0VCQuIQIry0Qqzuau8YlJVJqKQuw1ZuGgoPV2BjcRkKD1dA+7wDq7BvlLX9LwaZa42KYW3nn72AwsMVKM4uwFZuGqq9Y1BJXYbS1BzoeZ1cu/cR9MI6FB6uQGlqDlgOmAY6QyxwRuC7DXNDk546pH3egfzKUyhNzEA9NgQfUAw+oJhBykwe2TiZPO254YtST2ro118OeqwkJCQkhBhH6LhDZgi0kdkHtfgoId6JGSjOL0Hh4YpBzhuLy1CcXYDNm3eM/27lpqF84aohhViPiqukLhP5hVrw+WcvoDi/JCThrdw05F++Mi1wttlp0cuL80tQvnAVGqjPuLYWH4Ut9V//Yc/eQOeHHZ4qtg3P/k+854qEhITEDwdr8Ev4zcw+Q17ZRnHj+Dfz//XQ/x+3/P2MUa/aOwalqTkozi9BfuUpbCwuQ2liBmrxUagfS8E2ihubpZXUZdhYXDbPDWUWPs2bziSZrdy0cSD0BxSDbRSHau8YFGcXQM/rsP7X46U66s955ZKRBC4hIdFViNZTJSko4jq1+CiUL1yF8oWrFus8biPgemzIsL6tMo32eQfW374jxD81R+5B0wKwezRQH5QvXIXNm3eML4RK6jLVx8UkLglcQkKiK9G810rrxbTae8xTiGJDUJqYgfzKU8PThG1Wbiwuw+bNO1BWJummZZ/t+m3UA7X4qCH1FGcXiDRDrXs3Eq+hVKmO+nPSpVBCQqJr0VkyT9q8XZg+XpqYIdJJsQBao2IQ91ZuGiqpywZxWz1kGPkz6aU0MUPIe2rO5aDopP20I5R6Il0JJSQkDg2qKHGdRXhGk4/cKacwEq32jsFWbppshM4vwcbiskHa5QtXbaRtEnafTX9nfuOlqTkjkpNILCnHGaPMcmcWOYvQPOg+l5CQkIgc4wgdJ6Ho/Z+iIXJC4JXUZaj2jkG1d8zwdCkrkwZpm0FEJmlbyZ9Z3SzAiC0CTB9nvuF26cS8ZwP1QVmZhI3FZdDev//joPtZQkJCou1Y/+vxUj02FJ1VHhsyC9XGzWKSNr8A1GNDxF/9wlXYyk0bVjfbILVa2VbphC0Iht5Ow/XX9j7aQvQlJCQkDi1Wv/19rzQxw+nLrVvm7p4sfN2UQeKV1GVDI7fLJuZ9bORNvwCK80ugF9aJm6Il++KPeNSbhISERORY/fb3veL8ks0l0Gk9R6mhu1jxhvXex+n2SQd51+KjUJqas6cFYCH+IfKRS0hISBwKaIX1P3Vdg/zKU8Pdz7SK20zgQgvenozLGg1qSCc0D7noMImwJwRJSEhIdD20wvqf1iyD7KCG9bfvIP/sBeSfvTDc+9pF4qLNT6u3S37lqWl57zaE1rckcgkJiSMLrbD+J39wg0Hsex9hY3HZyH4YBXGLXA1Z5Gdpas7I4aKXN80EXMzyDkDeksglJCSOHGxEzhMhzWvCNhmDEzVP1havFeadokwaxK3rmuFpwvKp8IdENFMkkUtISBwJeBG5Xt4EvViArdw0TWhlj7TkCyPqWnzU5oVSmpiB0tQcbCwuE2ub5g43CjtNKOLj4SSRS0hIHAl4ErmugbbbIGdjTswYft1lZZKQMyXozZt3jIRV+WcvyNmZNN2sUSzZC60HJLezSCKXkJA4EvAkckbmjYpBwo5itaxZHXagRAfIWhK5hITEkYcvkVtLsWCWAyZpSeQSEhISFKGIvMuKJHIJCYkjAa2w/u+DJty2EblMmiUhIXHI8D9V9N5Xaz41dAAAAABJRU5ErkJggg=='  alt="FH Coburg Logo" height="66" width="370" align="right">
		<br>
		<title>CarreraBot</title>
		<!-- delete!!!!!!
		<link rel="stylesheet" href="style.css">
		-->
		<!--appearance configuration of the html-site-->
		<style>
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			margin: left;
			background: #eee;
		}
		input {
			display: block;
			margin: auto;
			width: 60%;
		}
		button {
			display: block;
		}
		.box {
			float: left;
			width: 20%;
			margin-right: 2%;
			padding: 20px;
			border: 1px solid black;
			background: #eee;
			box-sizing: border-box;;
		}
		.box:last-child {
			margin-right: 0;
			width: 55%; 
		}    
		.site {
			padding: 20px;
		}
		</style>
		<!--the main script to load the right values to the right place-->
		<script>
		 <!-- the function list get the new values (strings) seperate and place them in an variable "links or "rechts" -->
		 <!-- and concatinate it with an incremented number started by 0 until the function get no more values from the uC -->
		 function list() {
		   fetch('lots').then(function (response) {
			 return response.json();
		   }).then(function (json) {
			 for (var i = 0; i < json.length; i++) {
			   var listItem = document.createElement('span');
			   listItem.innerHTML = '<strong>' +  json[i].name + '</strong>';
			   document.getElementById("links" +i).appendChild(listItem);
			   
			 }
			 for (var i = 0; i < json.length; i++) {
			   var listItem = document.createElement('span');
			   listItem.setAttribute('id', 'wert' + i);
			   document.getElementById("rechts" +i).appendChild(listItem);
			 }
		   });
		 }
		 document.addEventListener('DOMContentLoaded', list);
		 
		 <!-- update the values -->
		 function update() {
		   fetch('lots').then(function (response) {
			 return response.json();
		   }).then(function (array) {
			 for (var i = 0; i < array.length; i++) {
			   if (array[0].wert != '') document.querySelector('#wert' + i).innerHTML = (array[i].wert);
			 }
		   });
		 }

		 document.addEventListener('DOMContentLoaded', update);
		 setInterval(update, 1000); <!-- change intervall to update race values slower or faster (in ms) -->
		</script>
	</head>
	<body>
		<p>
		<h1><strong>CarreraBot Webserver</strong></h1>
		</p>
		<hr>
		<div class="site">
			<div id="buttons" >
				<div class="box">
					<form action="/start">
					<h2>Start Button</h2>
					<button> Start </button>
					</form>
					<br>
				</div>
				<div class="box">
					<form action="/restart">  
					<h2>Restart Button</h2>
					<button> Restart </button>
					</form>
					<br>
				</div>
				<div class="box">
					<br><br><br><br><br><br>
					<!--enough space for new functions-->
				</div>
			</div>
		</div>	
		<p>
			<br><br><br><br><br><br><br><hr>
			<h2>Current Data</h2>
		</p>
		<div class="site">
		<!--Create the table for the race values-->
		<!--the variables "links" and "rechts" will be filled with values from the uC-->
			<table width=80%>
			<tr>
				<th><p id="links0"></p></th>
				<th><p id="links1"></p></th>
				<th><p id="links2"></p></th>   
				<th><p id="links3"></p></th>
				<th><p id="links4"></p></th>
				<th><p id="links5"></p></th>
				<td><p id=""></p></td>
				<th><p id="links6"></p></th>
				<th><p id="links7"></p></th>
				<th><p id="links8"></p></th>
				<th><p id="links9"></p></th>        
			</tr>
			<tr>
				<td><p id="rechts0"></p></td>
				<td><p id="rechts1"></p></td>
				<td><p id="rechts2"></p></td>
				<td><p id="rechts3"></p></td>
				<td><p id="rechts4"></p></td>
				<td><p id="rechts5"></p></td>
				<td><p id=""></p></td>
				<td><p id="rechts6"></p></td>
				<td><p id="rechts7"></p></td>
				<td><p id="rechts8"></p></td>
				<td><p id="rechts9"></p></td>            
			</tr>

			<tr>
				<td><p id=""></p></td>
				<td><p id=""></p></td>            
				<td><p id="links14"></p></td>
				<td><p id="links15"></p></td>
				<td><p id="links16"></p></td>
				<td><p id="links17"></p></td>
				<td><p id=""></p></td>             
				<td><p id="links10"></p></td>
				<td><p id="links11"></p></td>
				<td><p id="links12"></p></td>
				<td><p id="links13"></p></td>  
			</tr>             
			<tr>
				<td><p id=""></p></td>
				<td><p id=""></p></td>
				<td><p id="rechts14"></p></td>
				<td><p id="rechts15"></p></td>
				<td><p id="rechts16"></p></td>
				<td><p id="rechts17"></p></td>
				<td><p id=""></p></td>                                        
				<td><p id="rechts10"></p></td>
				<td><p id="rechts11"></p></td>
				<td><p id="rechts12"></p></td>
				<td><p id="rechts13"></p></td>            
			</tr>           
			</table> 
		</div>               
	</body>
</html>
)=====";




